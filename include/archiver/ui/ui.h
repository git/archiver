

/*
  Header file containing info about user interface

  Copyright (C) 2017  Charlie Sale

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef UI_H
#define UI_H

#include <ncurses.h>
#include <panel.h>
#include <form.h>
#include <menu.h>
#include <readline/readline.h>

// struct for holding parts of interface

extern const char** menu_items = {
  "New",
  "Open",
  "Decompress",
  "Quit"
};

typedef struct _archive_ui
{
  // represents option menu
  struct
  {
    MENU*  main_menu;  // menu with command data
    ITEM** cmd_items;  // commands to go in menu
    ITEM*  cur_item;   // item currently selected
  } main_menu;

  // represents command menu (add, remove file, etc)
  struct
  {
    MENU*  cmd_menu;  // menu
    ITEM** cmd_items; // items
    ITEM*  cur_item;  // current item
  } command_menu;

  // represents file view
  struct
  {
    WINDOW* wind;            // window of file viewer
    MENU*   file_menu;       // menu containing all of the files
    ITEM**  files;           // list of files
    ITEM*   cur_file;        // current selected file

    ITEM*** backup_files;    // lists of backuped files
    ITEM**  backup_cur_file; // stores last batchs
    int     backup_levels;   // levels of backup (as levels increase, number increases)
  } system_file;

  // used for viewing archive's contents
  struct
  {
    WINDOW* wind;              // window around menu
    MENU*   file_menu;         // actual menu
    ITEM**  files;             // list of files being displayed
    ITEM*   cur_file;          // pointer to currently selected file

    ITEM*** backup_files;      // list of backup files
    ITEM**  backup_cur_file;   // backup of most recently selected file
    int     backup_level;      // how many levels up it has gone
  } archive_viewer;

  // represents command line
  struct
  {
    WINDOW* window;    // window where editor will go
  } command_line;

} ui_t;

/* Begin decls */

/*
  Initilize user interface struct with some given data
 */
ui_t
create_ui(int, int);

void
destroy_ui();

#endif // UI_H
